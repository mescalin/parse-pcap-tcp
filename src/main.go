package main

import (
    "bytes"
    "encoding/binary"
    "errors"
    "flag"
    "fmt"
    "io"
    "log"
    "net"
    "os"

    "../dep/gopcap"
)

type (
    fb_package struct {
        len uint16
        command uint16
        data []byte
    }
)

var (
    pcap_file = flag.String("f", "", "pcap file")
)

func parse_fb_protocal(buf []byte) error {
    var e error
    b := bytes.NewBuffer(buf)
    index := 0
    for {
        if b.Len() == 0 {
            break
        }
        elt := fb_package{}
        if e = binary.Read(b, binary.LittleEndian, &elt.len); e != nil {
            break
        }
        if e = binary.Read(b, binary.LittleEndian, &elt.command); e != nil {
            break
        }
        data_len := elt.len - 4
        elt.data = make([]byte, data_len)
        if readed, e := b.Read(elt.data); e != nil {
            break
        } else if readed != int(data_len) {
            e = errors.New(fmt.Sprintf("readed != %d", data_len))
            break
        }
        index++
        log.Printf("(%d): len=%d, command=%x\n", index, elt.len, elt.command)
    }
    return e
}

func test() {
    b := bytes.NewBufferString("12345")
    buf := make([]byte, 10)
    n, err := b.Read(buf)
    log.Println(n, err)
}

func main() {
    flag.Parse()
    if *pcap_file == "" {
        flag.Usage()
        return
    }
    log.Println(*pcap_file)

    var f *os.File
    var err error
    if *pcap_file == "-" {
        f = os.Stdin
    } else {
        f, err = os.Open(*pcap_file)
    }

    if err != nil {
        log.Fatal(err)
    }

    flipped, link_type, err := gopcap.Parse2Init(f)
    if err != nil {
        log.Fatal(err)
    }

    for {
        packet, err := gopcap.Parse2(f, flipped, link_type)
        if err != nil {
            if err == io.EOF {
                break
            } else {
                log.Fatal(err)
            }
        }

        if ef, ok := packet.Data.(*gopcap.EthernetFrame); ok {
            if ipv4, ok := ef.LinkData().(*gopcap.IPv4Packet); ok {
                if tcp, ok := ipv4.InternetData().(*gopcap.TCPSegment); ok {
                    data := tcp.TransportData()

                    log.Printf("src=%v, dest=%v, len=%d\n",
                        net.IPv4(ipv4.SourceAddress[0], ipv4.SourceAddress[1], ipv4.SourceAddress[2], ipv4.SourceAddress[3]),
                        net.IPv4(ipv4.DestAddress[0], ipv4.DestAddress[1], ipv4.DestAddress[2], ipv4.DestAddress[3]),
                        len(data))

                    if err = parse_fb_protocal(data); err != nil {
                        log.Println(err)
                    }
                }
            }
        }
    }
}
